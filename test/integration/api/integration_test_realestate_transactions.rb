class IntegrationTestRealestateTransactions < ActionDispatch::IntegrationTest

	setup do  
		Rails.application.load_seed
  	end

  	test "pagination of realestate transaction is 10" do
      	get api_realestate_transactions_url
      	returned_transactions = JSON.parse(response.body)
      	assert_equal returned_transactions.count, 10

      	get api_realestate_transactions_url, page: 2
      	returned_transactions = JSON.parse(response.body)
      	assert_equal returned_transactions.count, 10
  	end

  	test "add a realeatate_transaction then edit then delete" do
  		# Create
      	post api_realestate_transactions_url, { realestate_transaction: { street: "My street", city: "My city", zip: "3000",
                                                  state: "CA", beds: 3, baths: 2, sq_feet: 1040, r_type: "Residential",
                                                  sale_date: "2017-05-19", price: "200000", longitude: "190.05", 
                                                  latitude: "29.239" }
                                           }
        assert(RealestateTransaction.count, +1)
        realestate_transaction = RealestateTransaction.last
        # Update
        put api_realestate_transaction_url( realestate_transaction), { realestate_transaction: { street: "my new street"}}
        realestate_transaction.reload
      	assert_equal realestate_transaction.street, "my new street"
      	## Delete
      	delete api_realestate_transaction_url( realestate_transaction)
      	assert_equal RealestateTransaction.where(street: "my new street").count, 0

  	end

  	test "searching by type and sq_feet range" do

	    get search_api_realestate_transactions_url, {r_type: "Residential", sq_feet: {from: 100, to: 2000}}
      	actual_result = RealestateTransaction.where(r_type: "Residential" , sq_feet: 100..2000)
      	returned_result = JSON.parse(response.body)

      	assert_equal actual_result.first.id, returned_result.first["id"]
      	assert_equal actual_result.count, returned_result.count
      	assert_response :success

      	get search_api_realestate_transactions_url, {r_type: "Condo", sq_feet: {from: 100, to: 2000}}
      	actual_result = RealestateTransaction.where(r_type: "Condo" , sq_feet: 100..2000)
      	returned_result = JSON.parse(response.body)
      	assert_equal actual_result.count, returned_result.count
      	assert_response :success
  	end
end