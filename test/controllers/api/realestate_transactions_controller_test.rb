class RealestateTransactionsControllerTest < ActionDispatch::IntegrationTest
  
  setup do
    @realestate_transaction = RealestateTransaction.create(street: "My street", city: "My city", zip: "3000",
                                state: "CA", beds: 3, baths: 2, sq_feet: 1040, r_type: "Residential",
                                sale_date: "2017-05-19", price: "200000", longitude: "190.05", 
                                latitude: "29.239")
  end

  test "should get index" do
      get api_realestate_transactions_url
      assert_response :success
  end

  test "should create new RealestateTransaction" do

	    assert_difference('RealestateTransaction.count') do
	    post api_realestate_transactions_url, { realestate_transaction: { street: "My street", city: "My city", zip: "3000",
                                                  state: "CA", beds: 3, baths: 2, sq_feet: 1040, r_type: "Residential",
                                                  sale_date: "2017-05-19", price: "200000", longitude: "190.05", 
                                                  latitude: "29.239" }
                                           }
	    end
	    assert_response :success
      assert(RealestateTransaction.count, +1)
  end

  test "should can update a realestate_transaction" do
      put api_realestate_transaction_url( @realestate_transaction), { realestate_transaction: { street: "my new street"}}
  	  assert_response :success
  	  @realestate_transaction.reload
      assert_equal @realestate_transaction.street, "my new street"
  end

  test "should destroy a realestate_transaction" do
	  assert_difference('RealestateTransaction.count', -1) do
	    delete api_realestate_transaction_url( @realestate_transaction)
	  end	 
	  assert_response :success
  end

  test "should search realestate_transactions by type and sq_feet range" do
      get search_api_realestate_transactions_url, {r_type: "Residential", sq_feet: {from: 100, to: 2000}}
      actual_result = RealestateTransaction.where(r_type: "Residential" , sq_feet: 100..2000)
      returned_result_id = JSON.parse(response.body).first["id"]
      assert_equal actual_result.first.id, returned_result_id
      assert_response :success
  end

end