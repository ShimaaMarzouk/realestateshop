# README #

This README would normally document whatever steps are necessary to get your application up and running.

#Ruby version
	using ruby 2.3.1
#creating application gemset
	rvm --create ruby-2.3.1@realestateshop
#creating application
	mkdir realestateshop
	cd realestateshop
	rvm gemset use ruby-2.3.1@realestateshop
	bundle install

#use of postgreSQL
	rake db:create
	rake db:migrate
	rake db:seed

#calling APIs:
	GET http://localhost:3000/api/realestate_transactions
		pagination per 10, use page param to determine page number
	POST http://localhost:3000/api/realestate_transactions
	PATCH http://localhost:3000/api/realestate_transactions/:id
	DELETE http://localhost:3000/api/realestate_transactions/:id
	GET http://localhost:3000/api/realestate_transactions/search
		r_type, sq_feet[from], sq_feet[to] params

#testing code run
	rake test test/controllers/api/realestate_transactions_controller_test.rb
	rake test test/integration/api/integration_test_realestate_transactions.rb


