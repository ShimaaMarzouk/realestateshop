module Api
  class RealestateTransactionsController < ApiController
    ## GET http://localhost:3000/api/realestate_transactions
    ## params:
    # page(number) : used for pagination
    def index
      realestate_transactions = RealestateTransaction.all
      # render json: @realestate_transactions
      paginate json: realestate_transactions, per_page: 10
    end

    ## POST http://localhost:3000/api/realestate_transactions
    ## params:
    #realestate.street = row['street']
    # realestate_transaction[city]
    # realestate_transaction[zip
    # realestate_transaction[state]
    # realestate_transaction[beds]
    # realestate_transaction[baths]
    # realestate_transaction[sq_feet]
    # realestate_transaction[r_type]
    # realestate_transaction[sale_date]
    # realestate_transaction[price]
    # realestate_transaction[longitude]
    # realestate_transaction[latitude]
    def create
      if RealestateTransaction.create(realestate_transaction_params)
        render :json=>{:success=>true}
      end
    end

    ## PATCH http://localhost:3000/api/realestate_transactions/:id
    def update
      realestate_transaction = RealestateTransaction.find(params[:id])
      if realestate_transaction.update_attributes(realestate_transaction_params)
        render :json=>{:success=>true}
      end
    end

    ## DELETE http://localhost:3000/api/realestate_transactions/:id
    def destroy
      realestate_transaction = RealestateTransaction.find(params[:id])
      if realestate_transaction.destroy
        render :json=>{:success=>true}
      end        
    end

    ## GET http://localhost:3000/api/realestate_transactions/search
    ## params:
    # r_type
    # sq_feet[from]
    # sq_feet[to]
    def search
      # Type (String)
      # Size (Range) ?????????
      # sq_feet (Range)
      r_type = params[:r_type]
      sq_feet_from = params[:sq_feet][:from]
      sq_feet_to = params[:sq_feet][:to]

      realestate_transactions = RealestateTransaction.with_type(r_type)
                                                      .with_sq_feet_range(sq_feet_from, sq_feet_to)
      render json: realestate_transactions
    end

    private

    def realestate_transaction_params
      params.require(:realestate_transaction).permit(:street, :city, :zip, :state, 
                                    :beds, :baths, :sq_feet, :r_type, 
                                    :sale_date, :price, :longitude, :latitude
                                  )
    end
  end
end
