class RealestateTransaction < ActiveRecord::Base
	
	scope :with_type, ->(r_type) { where(r_type: r_type) }
	scope :with_sq_feet_range, ->(from, to) { where(sq_feet: [from..to]) }
end