# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

require 'csv'

realestatetransactions_file = File.read(Rails.root.join('lib', 'seeds', 'Sacramentorealestatetransactions.csv'))
realestatetransactions = CSV.parse(realestatetransactions_file, :headers => true, :encoding => 'ISO-8859-1')

realestatetransactions.each do |row|
	realestate = RealestateTransaction.new
  	realestate.street = row['street']
  	realestate.city = row['city']
  	realestate.zip = row['zip']
  	realestate.state = row['state']
  	realestate.beds = row['beds']
  	realestate.baths = row['baths']
  	realestate.sq_feet = row['sq__ft']
  	realestate.r_type = row['type']
  	realestate.sale_date = row['sale_date']
  	realestate.price = row['price']
  	realestate.longitude = row['longitude']
  	realestate.latitude = row['latitude']
  	realestate.save
end