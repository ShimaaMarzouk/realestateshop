class AddRealestatetransaction < ActiveRecord::Migration
  def change
  	create_table :realestate_transaction do |t|
  		t.string :street
  		t.string :city
  		t.integer :zip
  		t.string :state
  		t.integer :beds
  		t.integer :baths
  		t.integer :sq_feet
  		t.string :r_type
  		t.date :sale_date
  		t.integer :price
  		t.float :longitude
  		t.float :latitude

  		t.timestamps
  	end
  end
end
